package INF101.lab2;

import java.util.List;
import java.util.ArrayList;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
	public ArrayList<FridgeItem> items;
	public int max;

	public Fridge() {
		this.items = new ArrayList<FridgeItem>();
		this.max = 20;
	}

	public int nItemsInFridge() {
		return items.size();
	}

	public int totalSize() {
		return max;
	}

	public boolean placeIn(FridgeItem item){
		if (items.size() < 20) {
			items.add(item);
			return true;
		} else {
			return false;
		}
	}
	
	public void takeOut(FridgeItem item) {
		if (items.contains(item)){
			items.remove(item);
		} else {
			throw new NoSuchElementException();
		}
	}

	public void emptyFridge() {
		items.clear();
	}

	public List<FridgeItem> removeExpiredFood() {
		List<FridgeItem> freshFood = new ArrayList<FridgeItem>();
		for (FridgeItem item: items) {
			if (item.hasExpired() == false) {
				freshFood.add(item);
			}
		}
        items.removeAll(freshFood);
		return items;
	}



	


}